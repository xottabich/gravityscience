﻿using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Отвечает за хранение не изменяемых во время игры данных, а также за их загрузку и обработку.
/// </summary>
public class StaticData : MonoBehaviour
{
    /// <summary>
    /// Объект Planets.
    /// </summary>
    public Planets planets;

    /// <summary>
    /// Объект Levels.
    /// </summary>
    public Levels levels;

    /// <summary>
    /// Объект HelpText.
    /// </summary>
    public HelpText texts;

    /// <summary>
    /// Загружает текстовый файл.
    /// </summary>
    public static string LoadResourceTextfile(string name)
    {
        string filePath = "JsonData/" + name;
        TextAsset targetFile = Resources.Load<TextAsset>(filePath);
        return targetFile.text;
    }

    /// <summary>
    /// Обертка планет (wrapper) для json.
    /// </summary>
    [Serializable]
    public class Planets
    {
        /// <summary>
        /// Список планет.
        /// </summary>
        public List<PlayObject> list;
    }

    /// <summary>
    /// Обертка уровней (wrapper) для json.
    /// </summary>
    [Serializable]
    public class Levels
    {
        /// <summary>
        /// Список уровней.
        /// </summary>
        public List<Level> list;
    }

    /// <summary>
    /// Обертка табличек помощи (wrapper) для json.
    /// </summary>
    [Serializable]
    public class HelpText
    {
        /// <summary>
        /// Список текстов.
        /// </summary>
        public List<Help> list;
    }

    /// <summary>
    /// Получает Sprite по названию.
    /// </summary>
    public Sprite GetSpriteByKeyName(string keyName)
    {
        Texture2D tex = Resources.Load(keyName) as Texture2D;
        if (tex == null)
        {
            return null;
        }
        Rect rec = new Rect(0, 0, tex.width, tex.height);
        return Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1);
    }

    /// <summary>
    /// Получает уровень по названию.
    /// </summary>
    public Level GetLevelByKeyName(string keyName)
    {
        foreach (var i in levels.list)
        {
            if (i.KeyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    /// <summary>
    /// Получает следующую планету.
    /// </summary>
    public int GetNextPlanet(int curPlanet)
    {
        int id = curPlanet;
        do
        {
            if (id + 1 < this.planets.list.Count - 1)
            {
                ++id;
            }
            else
            {
                id = 0;
            }
        } while (!(this.planets.list[id].type == "Planet"));
        return id;
    }

    /// <summary>
    /// Получает предыдущую планету.
    /// </summary>
    public int GetBackPlanet(int curPlanet)
    {
        int id = curPlanet;
        do
        {
            if (id - 1 < 0)
            {
                id = this.planets.list.Count - 1;
            }
            else
            {
                --id;
            }
        } while (!(this.planets.list[id].type == "Planet"));
        return id;
    }

    /// <summary>
    /// Получает планету по названию.
    /// </summary>
    public PlayObject GetPlanetByKeyName(string key_name)
    {
        int hash = key_name.GetHashCode();
        foreach (var i in planets.list)
        {
            if (i.keyNameHash == hash)
            {
                return i;
            }
        }
        return null;
    }

    /// <summary>
    /// Инициализация объектов из таблицы.
    /// </summary>
    private void Awake()
    {
        if (FindObjectsOfType(typeof(StaticData)).Length > 1)
        {
            Destroy(this.gameObject);
        }
        string json;
        DontDestroyOnLoad(this.gameObject);

        json = LoadResourceTextfile("Planets");
        json = "{\"list\":" + json + "}";
        planets = JsonUtility.FromJson<Planets>(json);

        foreach (var i in planets.list)
        {
            i.Init();
        }

        json = LoadResourceTextfile("Levels");
        json = "{\"list\":" + json + "}";
        levels = JsonUtility.FromJson<Levels>(json);

        json = LoadResourceTextfile("HelpText");
        json = "{\"list\":" + json + "}";
        texts = JsonUtility.FromJson<HelpText>(json);

        this.gameObject.GetComponent<Model>().Init();
    }

}
