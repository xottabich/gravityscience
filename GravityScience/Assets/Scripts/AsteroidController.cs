﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Контролирует поведение астероида.
/// </summary>
public class AsteroidController : MonoBehaviour
{
    /// <summary>
    /// Начальная позиция астероида.
    /// </summary>
    private Vector2 startPosition;

    /// <summary>
    /// Радиус описываемой окружности, при движении астероида.
    /// </summary>
    private float radius = 3;

    /// <summary>
    /// Вызывается в начале работы скрипта, инициализирует начальную позицию астероида.
    /// </summary>
    private void Start()
    {
        startPosition = this.transform.position;
    }

    /// <summary>
    /// Вызывается раз в кадр, меняет позицию астероида согласно выбранной траектории (окружность).
    /// </summary>
    private void Update()
    {
        this.transform.position = new Vector2(Mathf.Cos(Time.time), Mathf.Sin(Time.time)) * radius + startPosition;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("TYT");
        StaticData staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        Model model = GameObject.Find("[StaticData]").GetComponent<Model>();
        GameObject buttonStart = GameObject.Find("Start");
        model.Load();
        SceneManager.LoadSceneAsync(staticData.levels.list[model.NowLevel].KeyName);
        buttonStart.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName("Start");
    }
}
