﻿using UnityEngine;

/// <summary>
/// Инициализирует ракету при запуске сцены.
/// </summary>
public class RocketController : MonoBehaviour
{
    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private StaticData staticData;

    /// <summary>
    /// Вызывается в начале работы скрипта.
    /// </summary>
    private void Start()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        this.GetComponent<GravityController>().Init(staticData.GetPlanetByKeyName("Rocket"));
    }
}
