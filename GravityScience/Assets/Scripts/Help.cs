﻿using System;

/// <summary>
/// Сериализует тексты.
/// </summary>
[Serializable]
public class Help
{
    /// <summary>
    /// Текст.
    /// </summary>
    public string text;
}
