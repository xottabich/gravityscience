﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Контролирует меню "Exit".
/// </summary>
public class ExitController : MonoBehaviour {

    /// <summary>
    /// Кнопка выхода из игры.
    /// </summary>
    private GameObject yes;

    /// <summary>
    /// Кнопка выхода в главное меню.
    /// </summary>
    private GameObject no;

    /// <summary>
    /// Кнопка возвращения в главное меню.
    /// </summary>
    private GameObject back;

    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private GameObject staticData;

    /// <summary>
    /// Вызывается при загрузке скрипта.
    /// </summary>
    private void Awake()
    {
        // нахождения объектов на экране
        no = GameObject.Find("No");
        yes = GameObject.Find("Yes");
        back = GameObject.Find("Back");
        staticData = GameObject.Find("[StaticData]");


        // добавление слушателей в события
        no.GetComponent<Button>().onClick.AddListener(BackClick);
        yes.GetComponent<Button>().onClick.AddListener(YesClick);
        back.GetComponent<Button>().onClick.AddListener(BackClick);
    }

    /// <summary>
    /// Открывает главное меню.
    /// </summary>
    private void BackClick()
    {
        GameObject.Find("[StaticData]").GetComponent<Model>().LevelMenu = false;
        SceneManager.LoadSceneAsync("Menu");
    }

    /// <summary>
    /// Выходит из игры.
    /// </summary>
    private void YesClick()
    {
        Application.Quit();
    }
}
