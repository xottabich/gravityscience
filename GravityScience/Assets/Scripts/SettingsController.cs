﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Контролирует меню "Setting".
/// </summary>
public class SettingsController : MonoBehaviour
{
    /// <summary>
    /// Кнопка включения музыки.
    /// </summary>
    private GameObject on;

    /// <summary>
    /// Кнопка выключения музыки.
    /// </summary>
    private GameObject off;

    /// <summary>
    /// Кнопка выбора первого вида ракеты.
    /// </summary>
    private GameObject first;

    /// <summary>
    /// Кнопка выбора второго вида ракеты.
    /// </summary>
    private GameObject second;

    /// <summary>
    /// Кнопка возвращения в главное меню.
    /// </summary>
    private GameObject back;

    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private GameObject staticData;

    /// <summary>
    /// Вызывается при загрузке скрипта.
    /// </summary>
    private void Awake()
    {
        // нахождения объектов на экране
        on = GameObject.Find("On");
        off = GameObject.Find("Off");
        first = GameObject.Find("First");
        second = GameObject.Find("Second");
        back = GameObject.Find("Back");
        staticData = GameObject.Find("[StaticData]");


        // добавление слушателей в события
        on.GetComponent<Button>().onClick.AddListener(OnClick);
        off.GetComponent<Button>().onClick.AddListener(OffClick);
        first.GetComponent<Button>().onClick.AddListener(FirstClick);
        second.GetComponent<Button>().onClick.AddListener(SecondClick);
        back.GetComponent<Button>().onClick.AddListener(BackClick);
    }

    /// <summary>
    /// Открывает главное меню.
    /// </summary>
    private void BackClick()
    {
        GameObject.Find("[StaticData]").GetComponent<Model>().LevelMenu = false;
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// Включает музыку.
    /// </summary>
    private void OnClick()
    {
        staticData.GetComponent<AudioSource>().mute = false;
        PlayerPrefs.SetInt("Music", 1);
    }

    /// <summary>
    /// Выключает музыку.
    /// </summary>
    private void OffClick()
    {
        staticData.GetComponent<AudioSource>().mute = true;
        PlayerPrefs.SetInt("Music", 0);
    }

    /// <summary>
    /// Меняет стилистику ракеты на первый вариант.
    /// </summary>
    private void FirstClick()
    {
        PlayerPrefs.SetInt("RocketStyle", 1);
    }

    /// <summary>
    /// Меняет стилистику ракеты на второй вариант.
    /// </summary>
    private void SecondClick()
    {
        PlayerPrefs.SetInt("RocketStyle", 2);
    }

}
