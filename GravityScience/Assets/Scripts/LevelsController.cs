﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Контролирует меню уровней.
/// </summary>
public class LevelsController : MonoBehaviour
{
    /// <summary>
    /// Контент уровня.
    /// </summary>
    private GameObject levelsContent;

    /// <summary>
    /// Объект уровня.
    /// </summary>
    private GameObject level;

    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private StaticData staticData;

    /// <summary>
    /// Список кнопок уровней.
    /// </summary>
    private List<GameObject> levelsButtons;

    /// <summary>
    /// Инициализация меню уровней.
    /// </summary>
    public void Init()
    {
        levelsContent = GameObject.Find("LevelsContent");
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        level = Resources.Load("Level") as GameObject;
        levelsButtons = new List<GameObject>();
        this.transform.Find("Back").GetComponent<Button>().onClick.AddListener(BackClick);
        this.transform.Find("Go").GetComponent<Button>().onClick.AddListener(GoClick);
        Redraw();
    }

    /// <summary>
    /// Перерисовывает экран игрока.
    /// </summary>
    public void Redraw()
    {
        foreach (var i in levelsButtons)
        {
            Destroy(i);
        }
        levelsButtons.Clear();
        for (int i = 0; i < Mathf.Min(staticData.levels.list.Count, PlayerPrefs.GetInt("CanDo") + 1); ++i)
        {
            GameObject go = GameObject.Instantiate(level, this.transform.position, this.transform.rotation);
            go.transform.SetParent(levelsContent.transform);
            levelsButtons.Add(go);
            go.GetComponent<LevelController>().Init(i);
        }
    }

    /// <summary>
    /// Возвращает игрока в главное меню.
    /// </summary>
    private void BackClick()
    {
        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// Продолжает игру с момента последнего захода в неё.
    /// </summary>
    private void GoClick()
    {
        int lvl = PlayerPrefs.GetInt("CanGo");
        GameObject.Find("[StaticData]").GetComponent<Model>().NowLevel = lvl;
        SceneManager.LoadSceneAsync(staticData.levels.list[lvl].KeyName);
    }
}
