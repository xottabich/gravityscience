﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// Отрисовывает процесс вытягивания из меню и выставления планет на экране.
/// </summary>
public class DrawPlanet : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    /// <summary>
    /// Модель планеты.
    /// </summary>
    public GameObject prefab;

    /// <summary>
    /// Планета, которую тянет пользователь.
    /// </summary>
    private PlayObject curPlanet;

    /// <summary>
    /// Текущая планета.
    /// </summary>
    private GameObject curDrag;

    /// <summary>
    /// Текущая ракета.
    /// </summary>
    private GameObject nowRocket;

    /// <summary>
    /// Корзина для удаления планет.
    /// </summary>
    private Trash trash = null;

    /// <summary>
    /// Инициализация планеты, которую необходимо отрисовывать.
    /// </summary>
    public void Init(PlayObject planet)
    {
        curPlanet = planet;
    }

    /// <summary>
    /// Вызывается, когда пользователь начинает тянуть планету.
    /// </summary>
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!GameObject.Find("Canvas").GetComponent<GameController>().Fly)
        {
            GameObject go = GameObject.Instantiate(prefab, this.transform.position, this.transform.rotation);
            go.transform.parent = GameObject.Find("PlanetParent").transform;
            go.GetComponent<GravityController>().Init(curPlanet);
            go.transform.localScale = new Vector3(curPlanet.radius, curPlanet.radius, 1);
            curDrag = go;
            go.GetComponent<Image>().sprite = this.GetComponent<Image>().sprite;
        }
    }

    /// <summary>
    /// Вызывается, когда пользователь тянет планету.
    /// </summary>
    public void OnDrag(PointerEventData data)
    {
        if (!GameObject.Find("Canvas").GetComponent<GameController>().Fly)
        {
            var v3 = Input.mousePosition;
            v3.z = 10.0f;
            v3 = Camera.main.ScreenToWorldPoint(v3);
            curDrag.gameObject.transform.position = v3;
        }
    }

    /// <summary>
    /// Вызывается, когда пользователь заканчивает тянуть планету.
    /// </summary>
    public void OnEndDrag(PointerEventData eventData)
    {
        if (!GameObject.Find("Canvas").GetComponent<GameController>().Fly)
        {
            if (trash.IsMouseOver)
            {
                Destroy(curDrag.gameObject);
                return;
            }
            curDrag.GetComponent<GravityController>().plan.curPosition = curDrag.transform.position;
        }
    }

    /// <summary>
    /// Выполняется при загрузке скрипта, находит текущую планету и корзины.
    /// </summary>
    private void Awake()
    {
        nowRocket = GameObject.Find("Rocket");
        trash = (Object.FindObjectsOfType(typeof(Trash)) as Trash[])[0];
    }
}
