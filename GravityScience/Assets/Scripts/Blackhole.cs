﻿using UnityEngine;

/// <summary>
/// Контролирует поведение чёрной дыры.
/// </summary>
public class Blackhole : MonoBehaviour
{
    /// <summary>
    /// Вторая чёрная дыра. Объект, из которого должна вылетать ракета, после того, как она влетает в чёрную дыру.
    /// </summary>
    private GameObject otherBlackhole;

    /// <summary>
    /// Инициализация, находит вторую чёрную дыру.
    /// </summary>
    private void Start()
    {
        otherBlackhole = GameObject.Find("Blackhole2");
    }

    /// <summary>
    /// Обрабатывает столкновение с чёрной дырой, меняет позицию ракеты на позицию второй чёрной дыры.
    /// </summary>
    private void OnTriggerEnter2D(Collider2D other)
    {
        other.transform.position = otherBlackhole.transform.position;
    }
}
