﻿using UnityEngine;
using System;

/// <summary>
/// Игровые объекты.
/// </summary>
[Serializable]
public class PlayObject
{
    /// <summary>
    /// Гравитационная константа.
    /// </summary>
    static private float G = 1;

    /// <summary>
    /// Масса объекта.
    /// </summary>
    public float mass;

    /// <summary>
    /// Радиус объекта.
    /// </summary>
    public float radius;

    /// <summary>
    /// Название объекта.
    /// </summary>
    public string keyName;

    /// <summary>
    /// Хэш имени объекта.
    /// </summary>
    public int keyNameHash;

    /// <summary>
    /// Название картинки для объекта.
    /// </summary>
    public string sprite;

    /// <summary>
    /// Тип объекта.
    /// </summary>
    public string type;

    /// <summary>
    /// Реальнаяя масса объекта.
    /// </summary>
    public string realMass;

    /// <summary>
    /// Реальный радиус объекта.
    /// </summary>
    public string realRadius;

    /// <summary>
    /// Позиция объекта в пространстве.
    /// </summary>
    public Vector3 curPosition;

    /// <summary>
    /// Конструктор.
    /// </summary>
    public PlayObject(PlayObject obj)
    {
        this.mass = obj.mass;
        this.radius = obj.radius;
        this.keyName = obj.keyName;
        this.sprite = obj.sprite;
        this.type = obj.type;
        this.curPosition = obj.curPosition;
        Init();
    }

    /// <summary>
    /// Конструктор.
    /// </summary>
    public PlayObject(float mass, float radius, string keyName)
    {
        this.mass = mass;
        this.radius = radius;
        this.keyName = keyName;
        Init();
    }

    /// <summary>
    /// Конструктор.
    /// </summary>
    public PlayObject(string key)
    {
        mass = PlayerPrefs.GetFloat(key + "mass");
        radius = PlayerPrefs.GetFloat(key + "radius");
        keyName = PlayerPrefs.GetString(key + "keyName");
        sprite = PlayerPrefs.GetString(key + "sprite");
        type = PlayerPrefs.GetString(key + "type");
        float x = PlayerPrefs.GetFloat(key + "x");
        float y = PlayerPrefs.GetFloat(key + "y");
        float z = PlayerPrefs.GetFloat(key + "z");
        curPosition = new Vector3(x, y, z);
        Init();
    }

    /// <summary>
    /// Конструктор.
    /// </summary>
    protected PlayObject() { }

    /// <summary>
    /// Расчёт гравитационной силы для двух игровых объектов.
    /// </summary>
    public static float Gravity(PlayObject first, PlayObject second, float R)
    {
        return (G * first.mass * second.mass) / Mathf.Pow(0.01f * R, 2);
    }

    /// <summary>
    /// Сохранение объекта.
    /// </summary>
    public void SaveObj(string key)
    {
        PlayerPrefs.SetFloat(key + "mass", mass);
        PlayerPrefs.SetFloat(key + "radius", radius);
        PlayerPrefs.SetString(key + "keyName", keyName);
        PlayerPrefs.SetString(key + "sprite", sprite);
        PlayerPrefs.SetString(key + "type", type);
        PlayerPrefs.SetFloat(key + "x", curPosition.x);
        PlayerPrefs.SetFloat(key + "y", curPosition.y);
        PlayerPrefs.SetFloat(key + "z", curPosition.z);
    }

    /// <summary>
    /// Инициализация объекта.
    /// </summary>
    public void Init()
    {
        keyNameHash = keyName.GetHashCode();
    }
}
