﻿/// <summary>
/// Описание текущего уровня.
/// </summary>
[System.Serializable]
public class Level
{
    /// <summary>
    /// Название уровня.
    /// </summary>
    public string KeyName;

    /// <summary>
    /// Скорость газовой струи.
    /// </summary>
    public float rocketSpeed;

    /// <summary>
    /// Масса газов.
    /// </summary>
    public float gasMass;

    /// <summary>
    /// Ежесекудный расход массы газов.
    /// </summary>
    public float gasSpeed;

    /// <summary>
    /// Рекорд по количеству планет.
    /// </summary>
    public int record;
}
