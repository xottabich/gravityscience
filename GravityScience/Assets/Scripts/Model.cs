﻿using UnityEngine;

/// <summary>
/// Поддерживает текущее состояние игры.
/// </summary>
public class Model : MonoBehaviour
{
    /// <summary>
    /// Номер текущего уровня.
    /// </summary>
    private int nowLevel;

    /// <summary>
    /// Свойство доступа к nowLevel.
    /// </summary>
    public int NowLevel
    {
        get
        {
            return nowLevel;
        }

        set
        {
            nowLevel = value;
        }
    }

    /// <summary>
    /// Объект Save.
    /// </summary>
    private Save save;

    /// <summary>
    /// Свойство доступа к save.
    /// </summary>
    public Save _Save
    {
        get
        {
            return save;
        }

        set
        {
            save = value;
        }
    }

    /// <summary>
    /// Определяет, нужно ли открыть меню уровней.
    /// </summary>
    private bool levelMenu;

    /// <summary>
    /// Свойство доступа к levelMenu.
    /// </summary>
    public bool LevelMenu
    {
        get
        {
            return levelMenu;
        }

        set
        {
            levelMenu = value;
        }
    }

    /// <summary>
    /// Сохранение уровня.
    /// </summary>
    public void Save()
    {
        save.SaveObj("save");
    }

    /// <summary>
    /// Вызывается, при загрузке скрипта.
    /// </summary>
    public void Init()
    {
        Load();
    }

    /// <summary>
    /// Загрузка уровня.
    /// </summary>
    public void Load()
    {
        save = new Save("save");
    }
}
