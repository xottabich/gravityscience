﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

/// <summary>
/// Отвечает за работу и отрисовку игрового меню.
/// </summary>
public class GameController : MonoBehaviour
{
    /// <summary>
    /// Все объекты на экране с гравитацией.
    /// </summary>
    private GravityController[] all_object;

    /// <summary>
    /// Все планеты на экране.
    /// </summary>
    private List<GameObject> planets;

    /// <summary>
    /// Модель планеты.
    /// </summary>
    public GameObject prefab;

    /// <summary>
    /// Показывает, летит ли ракета.
    /// </summary>
    private bool fly = false;

    /// <summary>
    /// Свойство доступа к fly.
    /// </summary>
    public bool Fly
    {
        get
        {
            return fly;
        }
    }

    /// <summary>
    /// Педыдущая планета в списке.
    /// </summary>
    private GameObject buttonBack;

    /// <summary>
    /// Следующая планета в списке.
    /// </summary>
    private GameObject buttonForward;

    /// <summary>
    /// Текущая планета.
    /// </summary>
    private GameObject nowPlanet;

    /// <summary>
    /// Кнопка начала игры.
    /// </summary>
    private GameObject buttonStart;

    /// <summary>
    /// Текущая ракета.
    /// </summary>
    private GameObject nowRocket;

    /// <summary>
    /// Таблица, появляющаяся после выигрыша.
    /// </summary>
    private GameObject winTable;

    /// <summary>
    /// Кнопка повторения уровня.
    /// </summary>
    private GameObject buttonReturn;

    /// <summary>
    /// Кнопка выхода в главное меню.
    /// </summary>
    private GameObject buttonMenu;

    /// <summary>
    /// Кнопка загрузки следующего уровня.
    /// </summary>
    private GameObject buttonNextLevel;

    /// <summary>
    /// Кнопка выхода в главное меню.
    /// </summary>
    private GameObject buttonBackLevel;

    /// <summary>
    /// Таблица с характеристиками ракеты.
    /// </summary>
    private GameObject rocketTable;

    /// <summary>
    /// Таблица с характеристиками планеты.
    /// </summary>
    private TablePlanetController planetTable;

    /// <summary>
    /// Объект, в который надо прилететь для успешного завершения уровня.
    /// </summary>
    private InFinish finish;

    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private StaticData staticData;

    /// <summary>
    /// Планета на курсоре.
    /// </summary>
    private int curPlanet;

    /// <summary>
    /// Объект Model.
    /// </summary>
    private Model model;

    /// <summary>
    /// Показывает, была ли начата игра.
    /// </summary>
    private bool startFlag;

    /// <summary>
    /// Показывает, была ли нажата текущая планета.
    /// </summary>
    private bool nowPlanetClicked;

    /// <summary>
    /// Показывает, была ли нажата ракета.
    /// </summary>
    private bool rocketClicked;

    /// <summary>
    /// Объект Canvas.
    /// </summary>
    private GameObject canvas;

    /// <summary>
    /// Масса ракеты.
    /// </summary>
    private float nowMass;

    /// <summary>
    /// Текущий уровень.
    /// </summary>
    private Level nowLevel;

    /// <summary>
    /// Сила для полета ракеты в текущий момент.
    /// </summary>
    private float F;

    /// <summary>
    /// Масса оставшихся газов в текущий момент.
    /// </summary>
    private float mass;

    /// <summary>
    /// Время запуска ракеты.
    /// </summary>
    private float start;

    /// <summary>
    /// Количество планет на экране.
    /// </summary>
    private int planetCount;

    /// <summary>
    /// Рекорд по количеству планет на экране для текущего уровня.
    /// </summary>
    private int nowRecord;

    /// <summary>
    /// Количесво набранных за уровень звёзд.
    /// </summary>
    private int nowStars;


    /// <summary>
    /// Вызывается в начале работы скрипта.
    /// </summary>
	void Start()
    {

        startFlag = false;
        nowPlanetClicked = false;
        rocketClicked = false;

        // нахождение объектов на экране
        buttonBack = GameObject.Find("ButtonBack");
        buttonForward = GameObject.Find("ButtonForward");
        nowPlanet = GameObject.Find("NowPlanet");
        buttonStart = GameObject.Find("Start");
        nowRocket = GameObject.Find("Rocket");
        winTable = GameObject.Find("WinTable");
        buttonReturn = GameObject.Find("Return");
        buttonMenu = GameObject.Find("Menu");
        buttonNextLevel = GameObject.Find("Next");
        buttonBackLevel = GameObject.Find("Back");
        rocketTable = GameObject.Find("RocketTable");
        planetTable = GameObject.Find("PlanetTable").GetComponent<TablePlanetController>();
        finish = GameObject.Find("Finish").GetComponent<InFinish>();
        canvas = GameObject.Find("Canvas");

        // объявление методов, вызывающихся при нажатии на кнопки
        buttonBack.GetComponent<Button>().onClick.AddListener(PrevClick);
        buttonForward.GetComponent<Button>().onClick.AddListener(NextClick);
        buttonStart.GetComponent<Button>().onClick.AddListener(StartClick);
        buttonReturn.GetComponent<Button>().onClick.AddListener(StopClick);
        nowPlanet.GetComponent<Button>().onClick.AddListener(NowPlanetClick);
        buttonMenu.GetComponent<Button>().onClick.AddListener(MenuClick);
        buttonNextLevel.GetComponent<Button>().onClick.AddListener(NextLevelClick);
        buttonBackLevel.GetComponent<Button>().onClick.AddListener(MenuClick);
        nowRocket.GetComponent<Button>().onClick.AddListener(RocketTableClick);

        winTable.SetActive(false);
        rocketTable.SetActive(false);
        planetTable.gameObject.SetActive(false);

        // нахождение объекта StaticData
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        model = GameObject.Find("[StaticData]").GetComponent<Model>();

        nowRocket.GetComponent<GravityController>().plan.mass = staticData.planets.list[staticData.planets.list.Count - 1].mass;
        nowMass = nowRocket.GetComponent<GravityController>().plan.mass;

        if (PlayerPrefs.HasKey("RocketStyle"))
        {
            nowRocket.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName("Rocket" + PlayerPrefs.GetInt("RocketStyle"));
        }

        if (GameObject.Find("Canvas").GetComponent<HelpController>() == null)
        {
            model.Load();
        }

        nowLevel = staticData.GetLevelByKeyName(staticData.levels.list[model.NowLevel].KeyName);

        curPlanet = 0;

        foreach (var element in model._Save.levels)
        {
            if (element.name == staticData.levels.list[model.NowLevel].KeyName)
            {
                foreach (var i in element.planets)
                {
                    GameObject go = GameObject.Instantiate(prefab, this.transform.position, this.transform.rotation);
                    go.transform.parent = GameObject.Find("PlanetParent").transform;
                    go.GetComponent<GravityController>().Init(i);
                    go.transform.localScale = new Vector3(i.radius, i.radius, 1);
                    go.transform.position = i.curPosition;
                    go.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName(i.keyName);
                }
            }
        }

        PlayerPrefs.SetInt("CanGo", model.NowLevel);

        nowRocket.transform.localScale = new Vector3(staticData.planets.list[staticData.planets.list.Count - 1].radius*3/4, staticData.planets.list[staticData.planets.list.Count - 1].radius, 1);

        // обновление видов кнопок
        UpdateCurPlanet(curPlanet);
    }

    /// <summary>
    /// Открывает и закрывает информацию о ракете.
    /// </summary>
    private void RocketTableClick()
    {
        rocketClicked = !rocketClicked;
        if (rocketClicked)
        {
            rocketTable.SetActive(true);
        }
        else
        {
            rocketTable.SetActive(false);
        }
    }

    /// <summary>
    /// Открывает главное меню.
    /// </summary>
    private void MenuClick()
    {
        if (GameObject.Find("Canvas").GetComponent<HelpController>() == null)
        {
            model.LevelMenu = true;
        }
        else
        {
            model.LevelMenu = false;
        }
        SceneManager.LoadSceneAsync("Menu");
    }

    /// <summary>
    /// Запускает следующий уровень.
    /// </summary>
    private void NextLevelClick()
    {
        if (staticData.levels.list.Count - 1 > model.NowLevel)
        {
            model.NowLevel += 1;
            SceneManager.LoadSceneAsync(staticData.levels.list[model.NowLevel].KeyName);
        }
    }

    /// <summary>
    /// Открывает и закрывает информацию о текущей планете.
    /// </summary>
    private void NowPlanetClick()
    {
        nowPlanetClicked = !nowPlanetClicked;
        if (nowPlanetClicked)
        {
            planetTable.Redraw(staticData.planets.list[curPlanet]);
            planetTable.gameObject.SetActive(true);
        }
        else
        {
            planetTable.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Обновляет внешний вид меню выбора планет
    /// </summary>
    /// <param name="id">Номер текущей планеты.</param>
    private void UpdateCurPlanet(int id)
    {
        curPlanet = id;

        // отрисовка текущей планеты
        nowPlanet.GetComponent<DrawPlanet>().Init(staticData.planets.list[id]);
        nowPlanet.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName(staticData.planets.list[id].sprite);

        // отрисовка предыдущей планеты
        buttonBack.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName(staticData.planets.list[staticData.GetBackPlanet(curPlanet)].sprite);

        // отрисовка следующей планеты
        buttonForward.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName(staticData.planets.list[staticData.GetNextPlanet(curPlanet)].sprite);
    }

    /// <summary>
    /// Обрабатывает нажатие на предыдущую планету.
    /// </summary>
    private void PrevClick()
    {
        UpdateCurPlanet(staticData.GetBackPlanet(curPlanet));
        planetTable.Redraw(staticData.planets.list[curPlanet]);
    }

    /// <summary>
    /// Обрабатывает нажатие на следующую планету.
    /// </summary>
    private void NextClick()
    {
        UpdateCurPlanet(staticData.GetNextPlanet(curPlanet));
        planetTable.Redraw(staticData.planets.list[curPlanet]);
    }

    /// <summary>
    /// Обрабатывает начало полета ракеты.
    /// </summary>
    private void StartClick()
    {
        if (!startFlag)
        {
            if (GameObject.Find("Canvas").GetComponent<HelpController>() == null)
            {
                planets = new List<GameObject>();
                all_object = FindObjectsOfType(typeof(GravityController)) as GravityController[];
                foreach (var element in all_object)
                {
                    if (element.gameObject.tag == "Planet")
                    {
                        element.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
                    }
                }
                Save();
            }
            startFlag = true;
            nowRocket.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            fly = true;
            start = Time.time;
            buttonStart.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName("Stop");

        }
        else
        {
            StopClick();
        }
    }

    /// <summary>
    /// Вызывается каждую фиксированную рамку кадра.
    /// </summary>
    private void FixedUpdate()
    {
        if (fly)
        {
            mass = nowLevel.gasMass - nowLevel.gasSpeed * (Time.time - start);
            if (mass > 0)
            {
                F = (nowLevel.gasSpeed * nowLevel.rocketSpeed) / (nowMass + mass);
                F *= Time.deltaTime;
                nowRocket.GetComponent<Rigidbody2D>().mass = nowMass + mass;
                nowRocket.GetComponent<Rigidbody2D>().AddForce(nowRocket.transform.up * F);
            }
        }
    }

    /// <summary>
    /// Запускает текущий уровень заново.
    /// </summary>
    private void StopClick()
    {
        if (GameObject.Find("Canvas").GetComponent<HelpController>() == null)
        {
            model.Load();
            SceneManager.LoadSceneAsync(staticData.levels.list[model.NowLevel].KeyName);
            buttonStart.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName("Start");
        }
        else
        {
            SceneManager.LoadSceneAsync("Help");
        }
    }

    /// <summary>
    /// Сохраняет текущее расположение планет.
    /// </summary>
    private void Save()
    {
        GravityController[] all_object = Object.FindObjectsOfType(typeof(GravityController)) as GravityController[];
        LevelSave level = new LevelSave();
        level.name = staticData.levels.list[model.NowLevel].KeyName;

        foreach (var element in all_object)
        {
            if (element.plan.type == "Planet")
            {
                level.planets.Add(element.plan);
            }
        }

        for (int i = 0; i < model._Save.levels.Count; ++i)
        {
            if (model._Save.levels[i].name == staticData.levels.list[model.NowLevel].KeyName)
            {
                model._Save.levels[i] = level;
                model.Save();
                return;
            }
        }

        model._Save.levels.Add(level);
        model.Save();
    }

    /// <summary>
    /// Вызывается при выигрыше игрока.
    /// </summary>
    private void Win()
    {
        if (GameObject.Find("Canvas").GetComponent<HelpController>() == null)
        {
            if (PlayerPrefs.GetInt("CanDo") < model.NowLevel)
            {
                PlayerPrefs.SetInt("CanDo", model.NowLevel);
            }

            nowRecord = staticData.levels.list[model.NowLevel].record;
            nowStars = 0;
            planetCount = 0;
            GravityController[] all_object = Object.FindObjectsOfType(typeof(GravityController)) as GravityController[];
            foreach (var element in all_object)
            {
                if (element.plan.type == "Planet")
                {
                    ++planetCount;
                }
            }

            if (planetCount <= nowRecord)
            {
                nowStars = 3;
            }
            else
            {
                if (nowRecord + 1 == planetCount)
                {
                    nowStars = 2;
                }
                else
                {
                    if (nowRecord + 2 == planetCount)
                    {
                        nowStars = 1;
                    }
                }
            }

            if (!PlayerPrefs.HasKey("star" + model.NowLevel) || (PlayerPrefs.GetInt("star" + model.NowLevel) < nowStars))
            {
                PlayerPrefs.SetInt("star" + model.NowLevel, nowStars);
            }

            winTable.SetActive(true);

            GameObject.Find("WinText").GetComponent<Text>().text = nowStars.ToString();
        }
        else
        {
            model.LevelMenu = true;
            SceneManager.LoadSceneAsync("Menu");
        }
    }

    /// <summary>
    /// Вызывается раз в кадр, проверяет, финишировала ли ракета.
    /// </summary>
    private void Update()
    {
        if (finish.FinishFlag)
        {
            Win();
        }
        else
        {
            var v3 = nowRocket.transform.position;

            Vector3[] can = new Vector3[4];
            canvas.GetComponent<RectTransform>().GetWorldCorners(can);
            Rect canRect = new Rect(can[0], can[2] - can[0]);

            if (!(canRect.Contains(v3)))
            {
                StopClick();
            }
        }
    }
}
