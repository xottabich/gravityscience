﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Контролирует кнопку меню уровней.
/// </summary>
public class LevelController : MonoBehaviour
{
    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private StaticData staticData;

    /// <summary>
    /// Инициализация.
    /// </summary>
    /// <param name="levelId">Номер уровня.</param>
    public void Init(int levelId)
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        int buffer = levelId;
        this.transform.Find("Text").GetComponent<Text>().text = staticData.levels.list[levelId].KeyName;
        if (PlayerPrefs.HasKey("star" + levelId))
        {
            this.transform.Find("Star").GetComponent<Text>().text = PlayerPrefs.GetInt("star" + levelId).ToString();
        }
        else
        {
            this.transform.Find("Star").GetComponent<Text>().text = "0";
        }
        this.gameObject.GetComponent<Button>().onClick.AddListener(delegate { LoadLevelClick(buffer); });
        this.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
    }

    /// <summary>
    /// Загрузка уровня.
    /// </summary>
    /// <param name="lvl">Номер уровня в списке.</param>
    private void LoadLevelClick(int lvl)
    {
        if (PlayerPrefs.GetInt("CanDo") >= lvl - 1)
        {
            GameObject.Find("[StaticData]").GetComponent<Model>().NowLevel = lvl;
            SceneManager.LoadSceneAsync(staticData.levels.list[lvl].KeyName);
        }
    }

}
