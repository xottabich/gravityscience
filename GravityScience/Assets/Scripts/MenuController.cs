﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Контролирует главное меню.
/// </summary>
public class MenuController : MonoBehaviour
{

    /// <summary>
    /// Кнопка перехода в меню уровней.
    /// </summary>
    private GameObject start;

    /// <summary>
    /// Кнопка перехода в меню "Help".
    /// </summary>
    private GameObject help;

    /// <summary>
    /// Кнопка перехода в меню "Setting".
    /// </summary>
    private GameObject settings;

    /// <summary>
    /// Кнопка перехода в меню "Exit".
    /// </summary>
    private GameObject exit;

    /// <summary>
    /// Контроллер уровней.
    /// </summary>
    private LevelsController chooseLevel;

    /// <summary>
    /// Объект Model.
    /// </summary>
    private Model model;

    /// <summary>
    /// Вызывается при загрузке скрипта.
    /// </summary>
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("CanDo"))
        {
            PlayerPrefs.SetInt("CanDo", 0);
        }
        if (PlayerPrefs.HasKey("Music") && PlayerPrefs.GetInt("Music") == 0)
        {
            GameObject.Find("[StaticData]").GetComponent<AudioSource>().mute = true;
        }
        if (!PlayerPrefs.HasKey("CanGo"))
        {
            PlayerPrefs.SetInt("CanGo", 0);
        }
        if (!PlayerPrefs.HasKey("RocketStyle"))
        {
            PlayerPrefs.SetInt("RocketStyle", 2);
        }

        // нахождение объектов на экране
        start = GameObject.Find("Start");
        help = GameObject.Find("Help");
        settings = GameObject.Find("Settings");
        exit = GameObject.Find("Exit");
        chooseLevel = GameObject.Find("ChooseLevel").GetComponent<LevelsController>();
        chooseLevel.Init();
        model = GameObject.Find("[StaticData]").GetComponent<Model>();
        model._Save = new Save();

        if (!model.LevelMenu)
        {
            chooseLevel.gameObject.SetActive(false);
        }


        // объявление методов, вызывающихся при нажатии на кнопки
        start.GetComponent<Button>().onClick.AddListener(StartClick);
        help.GetComponent<Button>().onClick.AddListener(HelpClick);
        settings.GetComponent<Button>().onClick.AddListener(SettingsClick);
        exit.GetComponent<Button>().onClick.AddListener(ExitClick);
    }

    /// <summary>
    /// Открывает меню уровней.
    /// </summary>
    private void StartClick()
    {
        chooseLevel.gameObject.SetActive(true);
        chooseLevel.Redraw();
    }

    /// <summary>
    /// Открывает меню "Help".
    /// </summary>
    private void HelpClick() {
        SceneManager.LoadSceneAsync("Help");
    }

    /// <summary>
    /// Открывает меню "Setting".
    /// </summary>
    private void SettingsClick()
    {
        SceneManager.LoadSceneAsync("Settings");
    }

    /// <summary>
    /// Открывает меню "Exit".
    /// </summary>
    private void ExitClick()
    {
        SceneManager.LoadSceneAsync("Exit");
    }
}
