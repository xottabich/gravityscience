﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Отслеживает нахождение элементов в зоне финиша.
/// </summary>
public class InFinish : MonoBehaviour
{
    /// <summary>
    /// Определяет, пересекла ли ракета финиш.
    /// </summary>
    private bool finishFlag = false;

    /// <summary>
    /// Свойство доступа к finishFlag.
    /// </summary>
    public bool FinishFlag
    {
        get
        {
            return finishFlag;
        }
    }

    /// <summary>
    /// Вызывается, когда какой-то предмет оказывается в зоне финиша.
    /// </summary>
    private void OnTriggerEnter2D(Collider2D other)
    {
        other.GetComponent<Image>().enabled = false;
        finishFlag = true;
    }
}
