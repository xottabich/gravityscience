﻿using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// Контролирует текстовые таблички в меню "Help".
/// </summary>
public class HelpController : MonoBehaviour
{
    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private StaticData staticData;

    /// <summary>
    /// Таблица для вывода текста.
    /// </summary>
    private GameObject helpTextTable;

    /// <summary>
    /// Поле для текста.
    /// </summary>
    private GameObject text;

    /// <summary>
    /// Кнопка перехода к следующей карточке.
    /// </summary>
    private GameObject stop;

    /// <summary>
    /// Номер таблички с текстом.
    /// </summary>
    private int id;

    /// <summary>
    /// Вызывается в начале работы скрипта.
    /// </summary>
    private void Start()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        helpTextTable = GameObject.Find("HelpText");
        text = GameObject.Find("HText");
        stop = GameObject.Find("Stop");

        stop.GetComponent<Button>().onClick.AddListener(StopClick);

        id = 0;
        PutTextAndWait(id);
    }

    /// <summary>
    /// Вставляет нужный текст в таблицу.
    /// </summary>
    /// <param name="id">Номер таблицы.</param>
    private void PutTextAndWait(int id)
    {
        text.GetComponent<Text>().text = staticData.texts.list[id].text;
    }

    /// <summary>
    /// Обновляет таблцу с текстом.
    /// </summary>
    private void StopClick()
    {
        ++id;
        if(id >= staticData.texts.list.Count)
        {
            helpTextTable.SetActive(false);
            return;
        }
        PutTextAndWait(id);
    }

}
