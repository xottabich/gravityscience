﻿using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Поддерживает глобальное сохранение.
/// </summary>
[Serializable]
public class Save
{
    /// <summary>
    /// Список уровней.
    /// </summary>
    public List<LevelSave> levels = new List<LevelSave>();

    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private StaticData staticData;

    /// <summary>
    /// Конструктор.
    /// </summary>
    public Save()
    {

    }

    /// <summary>
    /// Конструктор с параметром.
    /// </summary>
    public Save(string key)
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        if (PlayerPrefs.HasKey(key + "couLevels"))
        {
            int cou = PlayerPrefs.GetInt(key + "couLevels");
            for (int i = 0; i < cou; i++)
            {
                levels.Add(new LevelSave(key + staticData.levels.list[i].KeyName));
            }
        }
    }

    /// <summary>
    /// Сохранение объекта.
    /// </summary>
    /// <param name="key"></param>
    public void SaveObj(string key)
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        PlayerPrefs.SetInt(key + "couLevels", levels.Count);
        for (int i = 0; i < levels.Count; i++)
        {
            levels[i].SaveObj(key + staticData.levels.list[i].KeyName);
        }
    }

}
