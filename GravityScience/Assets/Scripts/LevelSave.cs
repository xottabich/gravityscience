﻿using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Поддерживает сохранение уровня.
/// </summary>
[Serializable]
public class LevelSave
{
    /// <summary>
    /// Список планет.
    /// </summary>
    public List<PlayObject> planets = new List<PlayObject>();

    /// <summary>
    /// Название уровня.
    /// </summary>
    public string name;

    /// <summary>
    /// Сохранение объекта.
    /// </summary>
    /// <param name="key"></param>
    public void SaveObj(string key)
    {
        PlayerPrefs.SetString(key + "name", name);
        PlayerPrefs.SetInt(key + "planCou", planets.Count);
        for (int i = 0; i < planets.Count; i++)
        {
            planets[i].SaveObj(key + "planet" + i);
        }
    }
    
    /// <summary>
    /// Пустой конструктор.
    /// </summary>
    public LevelSave()
    {

    }

    /// <summary>
    /// Конструктор с парметром.
    /// </summary>
    public LevelSave(string key)
    {
        name = PlayerPrefs.GetString(key + "name");
        int cou = PlayerPrefs.GetInt(key + "planCou");
        for (int i = 0; i < cou; i++)
        {
            planets.Add(new PlayObject(key + "planet" + i));
        }
    }
}
