﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Контролирует таблицу с характеристиками планеты.
/// </summary>
public class TablePlanetController : MonoBehaviour
{

    /// <summary>
    /// Радиус планеты.
    /// </summary>
    private Text radius;

    /// <summary>
    /// Масса планеты.
    /// </summary>
    private Text mass;

    /// <summary>
    /// Название планеты.
    /// </summary>
    private Text name;

    /// <summary>
    /// Перерисовка таблицы.
    /// </summary>
    public void Redraw(PlayObject planet)
    {
        name.text = "Name: " + planet.keyName;
        mass.text = "Mass: " + planet.mass + "; Real mass: " + planet.realMass;
        radius.text = "Radius: " + planet.radius + "; Real radius: " + planet.realRadius;
    }

    /// <summary>
    /// Вызывается при загрузке скрипта.
    /// </summary>
    private void Awake()
    {
        radius = GameObject.Find("Radius").GetComponent<Text>();
        mass = GameObject.Find("Mass").GetComponent<Text>();
        name = GameObject.Find("Name").GetComponent<Text>();
    }

}
