﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Корзина для удаления игровых объектов.
/// </summary>
public class Trash : MonoBehaviour
{
    /// <summary>
    /// Определяет, находится ли мышка в корзине.
    /// </summary>
    private bool isMouseOver;

    /// <summary>
    /// Свойство доступа к isMouseOver.
    /// </summary>
    public bool IsMouseOver
    {
        get
        {
            return isMouseOver;
        }
    }

    /// <summary>
    /// Объект Canvas.
    /// </summary>
    private GameObject canvas;

    /// <summary>
    /// Корзина для удаления игровых объектов.
    /// </summary>
    private GameObject buttonTrash;

    /// <summary>
    /// Вызывается при загрузе скрипта.
    /// </summary>
    private void Awake()
    {
        canvas = GameObject.Find("Canvas");
        buttonTrash = GameObject.Find("Trash");
        buttonTrash.GetComponent<Button>().onClick.AddListener(ClearAll);
    }

    /// <summary>
    /// Очистка игрового поля.
    /// </summary>
    private void ClearAll()
    {
        if (!GameObject.Find("Canvas").GetComponent<GameController>().Fly)
        {
            GravityController[] all_object = Object.FindObjectsOfType(typeof(GravityController)) as GravityController[];
            foreach (var element in all_object)
            {
                if (element.plan.type == "Planet")
                {
                    Destroy(element.gameObject);
                }
            }
        }
    }

    /// <summary>
    /// Вызывается раз в кадр.
    /// </summary>
    private void Update()
    {
        var v3 = Input.mousePosition;
        v3.z = 10.0f;
        v3 = Camera.main.ScreenToWorldPoint(v3);

        Vector3[] corners = new Vector3[4];
        Vector3[] can = new Vector3[4];
        this.GetComponent<RectTransform>().GetWorldCorners(corners);
        Rect newRect = new Rect(corners[0], corners[2] - corners[0]);
        canvas.GetComponent<RectTransform>().GetWorldCorners(can);
        Rect canRect = new Rect(can[0], can[2] - can[0]);

        isMouseOver = newRect.Contains(v3) || !(canRect.Contains(v3));
    }
}
