﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Контроль всех объектов, имеющих гравитацию.
/// </summary>
public class GravityController : MonoBehaviour, IDragHandler, IEndDragHandler
{
    /// <summary>
    /// Список объектов, которые имеют гравитацию.
    /// </summary>
    private List<GravityController> can_gravity = new List<GravityController>();

    /// <summary>
    /// Список всех игровых объектов.
    /// </summary>
    private GravityController[] all_object;

    /// <summary>
    /// Текущая планета.
    /// </summary>
    public PlayObject plan;

    /// <summary>
    /// Текущий объект.
    /// </summary>
    private Rigidbody2D physicBody;

    /// <summary>
    /// Объект GameController.
    /// </summary>
    private GameController GC;

    /// <summary>
    /// Корзина для удаления планет.
    /// </summary>
    private Trash trash = null;

    /// <summary>
    /// Определяет, может ли двигаться объект.
    /// </summary>
    public bool canMoves;

    /// <summary>
    /// Вызывается, когда пользователь тянет планету.
    /// </summary>
    public void OnDrag(PointerEventData data)
    {
        if ((!this.GetComponent<GravityController>().canMoves) && (!GC.Fly))
        {
            var v3 = Input.mousePosition;
            v3.z = 10.0f;
            v3 = Camera.main.ScreenToWorldPoint(v3);
            this.gameObject.transform.position = v3;
        }
    }

    /// <summary>
    /// Вызывается, когда пользователь заканчивает тянуть планету.
    /// </summary>
    public void OnEndDrag(PointerEventData eventData)
    {
        plan.curPosition = this.transform.position;
        if (trash.IsMouseOver)
        {
            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// Инициализирует объект.
    /// </summary>
    public void Init(PlayObject planet)
    {
        plan = new PlayObject(planet);
    }

    /// <summary>
    /// Вызывается в начале работы скрипта.
    /// </summary>
    private void Start()
    {
        all_object = FindObjectsOfType(typeof(GravityController)) as GravityController[];
        physicBody = this.gameObject.GetComponent<Rigidbody2D>();
        GC = GameObject.Find("Canvas").GetComponent<GameController>();

        foreach (var element in all_object)
        {
            if (element != this)
            {
                can_gravity.Add(element);
                element.can_gravity.Add(this);
            }

        }

        trash = (FindObjectsOfType(typeof(Trash)) as Trash[])[0];
    }

    /// <summary>
    /// Вызывается раз в кадр.
    /// </summary>
    private void Update()
    {
        foreach (var element in can_gravity)
        {
            if (element != null)
            {
                Vector2 direction = element.transform.position - this.transform.position;
                direction = direction.normalized;
                if (canMoves)
                {
                    physicBody.AddForce(direction * PlayObject.Gravity(plan, element.plan, Vector2.Distance(this.transform.position, element.transform.position)));
                }
            }
        }
    }
}
