﻿using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// Контролирует таблицу с характеристиками ракеты.
/// </summary>
public class TableRocketController : MonoBehaviour
{
    /// <summary>
    /// Объект StaticData.
    /// </summary>
    private StaticData staticData;

    /// <summary>
    /// Объект Model.
    /// </summary>
    private Model model;

    /// <summary>
    /// Текущий уровень.
    /// </summary>
    private Level nowLevel;

    /// <summary>
    /// Вызывается в начале работы скрипта.
    /// </summary>
    private void Start()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        model = GameObject.Find("[StaticData]").GetComponent<Model>();
        nowLevel = staticData.GetLevelByKeyName(staticData.levels.list[model.NowLevel].KeyName);

        GameObject.Find("RocketMass").GetComponent<Text>().text = "Rocket's mass: " + staticData.planets.list[staticData.planets.list.Count - 1].mass;
        GameObject.Find("RocketSpeed").GetComponent<Text>().text = "Gas-stream velocity: " + nowLevel.rocketSpeed;
        GameObject.Find("GasMass").GetComponent<Text>().text = "Gas mass: " + nowLevel.gasMass;
        GameObject.Find("GasSpeed").GetComponent<Text>().text = "Every second mass flow: " + nowLevel.gasSpeed;
    }
}
